import 'package:flutter/material.dart';

import 'package:flutter_app/task.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

typedef ToggleTodoCallback = void Function(Task, bool);

class TodoList extends StatelessWidget {
  final CollectionReference taskList =
      Firestore.instance.collection("task");
  TodoList({@required this.todos, this.onTodoToggle});

  final List<Task> todos;
  final ToggleTodoCallback onTodoToggle;

  Widget _buildItem(BuildContext context, int index) {
    final todo = todos[index];

    return CheckboxListTile(
      value: todo.isDone,
      title: Text(todo.title),
      subtitle: Text(todo.description),
      onChanged: (bool isChecked) {
        onTodoToggle(todo, isChecked);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: _buildItem,
      itemCount: todos.length,
    );
  }
}