import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_app/authentication/login_signup_page.dart';

import 'package:flutter_app/task.dart';
import 'package:flutter_app/new_todo_dialog.dart';
import 'package:flutter_app/todo_list.dart';
import 'package:flutter_app/authentication/authentification.dart';

class TodoListScreen extends StatefulWidget {
  @override
  _TodoListScreenState createState() => _TodoListScreenState();
}

class _TodoListScreenState extends State<TodoListScreen> {
  List<Task> todos = [];
  FirebaseUser currentUser;

  _toggleTodo(Task todo, bool isChecked) {
    setState(() {
      todo.isDone = isChecked;
    });


  }

  _addTodo() async {
    final todo = await showDialog<Task>(
        context: context,
        builder: (BuildContext context) {
            return NewTodoDialog();
        },
    );

    if (todo != null) {
      setState(() {
        todos.add(todo);
      });
    }
  }

  void getCurrentUser() async {
    currentUser = await FirebaseAuth.instance.currentUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text('Todo List'),
            actions: <Widget>[
              FlatButton(
                textColor: Colors.white,
                onPressed: () {
                  FirebaseAuth.instance
                      .signOut()
                      .then((result) =>
                      Navigator.pushReplacementNamed(context, "/login"))
                      .catchError((err) => print(err) );
                },
                child: Text("Log Out"),
                shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
              ),
          ],
        ),
        body: DefaultTabController(
          length: 5,
          child: new Scaffold(
            body: TabBarView(
              children: [
                new Container(
                  color: Colors.white,
                  child: TodoList(
                    todos: todos,
                    onTodoToggle: _toggleTodo,
                  )
                ),
                new Container(
                  color: Colors.yellow,
                ),
                new Container(
                  color: Colors.orange,
                ),
                new Container(
                  color: Colors.lightGreen,
                ),
                new Container(
                  color: Colors.red,
                ),
              ],
            ),
            backgroundColor: Colors.black,
            bottomNavigationBar: new TabBar(
              tabs: [
                Tab(
                  icon: new Icon(Icons.home),
                ),
                Tab(
                  icon: new Icon(Icons.filter_1),
                ),
                Tab(
                  icon: new Icon(Icons.filter_2),
                ),
                Tab(
                  icon: new Icon(Icons.filter_3),
                ),
                Tab(
                  icon: new Icon(Icons.filter_4),
                )
              ],
              labelColor: Colors.yellow,
              unselectedLabelColor: Colors.blue,
              indicatorSize: TabBarIndicatorSize.label,
              indicatorPadding: EdgeInsets.all(5.0),
              indicatorColor: Colors.red,
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
             child: Icon(Icons.add),
             onPressed: _addTodo,
           ),
    );
  }
}
