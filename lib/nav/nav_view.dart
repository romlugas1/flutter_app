import 'package:flutter/material.dart';
import 'navigation.dart';

class NavigationView extends StatefulWidget {
  const NavigationView({ Key key, this.navigation }) : super(key: key);

  final Navigation navigation;

  @override
  _NavigationViewState createState() => _NavigationViewState();
}

class _NavigationViewState extends State<NavigationView> {
  TextEditingController _textController;

  @override
  void initState() {
    super.initState();
    _textController = TextEditingController(
      text: 'sample text: ${widget.navigation.title}',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${widget.navigation.title} Text'),
        backgroundColor: widget.navigation.color,
      ),
      backgroundColor: widget.navigation.color[100],
      body: Container(
        padding: const EdgeInsets.all(32.0),
        alignment: Alignment.center,
        child: TextField(controller: _textController),
      ),
    );
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }
}