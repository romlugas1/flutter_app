import 'package:flutter/material.dart';

class Navigation {
  const Navigation(this.title, this.icon, this.color);
  final String title;
  final IconData icon;
  final MaterialColor color;
}


//TODO: Implémenter la navbar

const List<Navigation> allNavigations = <Navigation>[
  Navigation('Home', Icons.home, Colors.teal),
  Navigation('Business', Icons.business, Colors.cyan),
  Navigation('School', Icons.school, Colors.orange),
  Navigation('Flight', Icons.flight, Colors.blue)
];