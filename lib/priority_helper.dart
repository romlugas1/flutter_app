import 'package:flutter/material.dart';

import 'package:flutter_app/priority.dart';

class PriorityHelper {
  static Color getPriorityColor(Priority priority) {
    switch (priority) {
      case Priority.Level4:
        return Colors.redAccent;

      case Priority.Level3:
        return Colors.amber;

      case Priority.Level2:
        return Colors.blueAccent;

      default:
        return Colors.lightGreen;
    }
  }

  static Priority toPriority(String value) {
    return Priority.values
        .firstWhere((priority) => priority.toString() == value);
  }
}
