import 'package:flutter/material.dart';
import 'package:flutter_app/priority.dart';

class Task {
  final String id;
  final String uid;
  final String title;
  final String description;
  final Priority priority;
  bool isDone;

  Task({
    this.id,
    this.uid,
    @required this.title,
    this.description,
    this.priority = Priority.Level1,
    this.isDone = false
  });
}