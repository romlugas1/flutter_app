import 'package:flutter/material.dart';

class PopupMenuInAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child : PopupMenuButton(
              icon: Icon(Icons.menu),
              itemBuilder: (context) => [
                PopupMenuItem(
                  child: Text("InduceSmile.com"),
                ),
                PopupMenuItem(
                  child: Text("Flutter.io"),
                ),
                PopupMenuItem(
                  child: Text("Google.com"),
                ),
              ]
            ),
        ),
    );
  }
}