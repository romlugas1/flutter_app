import 'package:flutter/material.dart';

import 'package:flutter_app/task.dart';

class NewTodoDialog extends StatefulWidget {
  @override
  _NewTodoDialogState createState() => _NewTodoDialogState();
}

class _NewTodoDialogState extends State<NewTodoDialog> {
  final titleController = new TextEditingController();
  final descriptionController = new TextEditingController();

  List<String> _priorities = <String>['Level1', 'Level2', 'Level3', "Level4"];
  String _priority;

  String titleValidator(String value) {
    if (value.length < 3)
      return 'Please enter a correct title (minimum 3 characters)';
    else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('New task'),
      content: Column(
        children: <Widget>[
          Expanded(
            child: TextFormField(
              controller: titleController,
              autofocus: true,
              decoration: InputDecoration(labelText: 'Task Title*'),
              validator: titleValidator,
            ),
          ),
          Expanded(
            child: TextField(
              controller: descriptionController,
              decoration: InputDecoration(labelText: 'Task Description'),
            )
          ),
          FormField(
            builder: (FormFieldState state) {
              return InputDecorator(
                decoration: InputDecoration(
                  labelText: 'Priotity',
                ),
                isEmpty: _priority == '',
                child: new DropdownButtonHideUnderline(
                  child: new DropdownButton(
                    value: _priority,
                    //isDense: true,
                    onChanged: (String newValue) {
                      setState(() {
                        _priority = newValue;
                        state.didChange(newValue);
                      });
                    },
                    items: _priorities.map((String value) {
                      return new DropdownMenuItem(
                        value: value,
                        child: new Text(value),
                      );
                    }).toList(),
                  ),
                ),
              );
            },
          ),

        ],
      ),

      actions: <Widget>[
        FlatButton(
          child: Text('Cancel'),
          onPressed: () {
            titleController.clear();
            descriptionController.clear();
            Navigator.pop(context);
          },
        ),
        FlatButton(
            child: Text('Add'),
            onPressed: () {
              final todo = new Task(
                  title: titleController.value.text,
                  description: descriptionController.value.text
              );
              titleController.clear();
              descriptionController.clear();
              Navigator.of(context).pop(todo);
            }
        )
      ],
    );
  }
}