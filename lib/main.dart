import 'package:flutter/material.dart';
import 'package:flutter_app/authentication/login_signup_page.dart';
import 'package:flutter_app/authentication/root_page.dart';
import 'package:flutter_app/authentication/authentification.dart';
import 'package:flutter_app/todo_list_screen.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter login demo',
        debugShowCheckedModeBanner: false,
        theme: new ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: new RootPage(auth: new Auth()),
        routes: <String, WidgetBuilder>{
          '/task': (BuildContext context) => TodoListScreen(),
          '/login': (BuildContext context) => LoginSignupPage(),
        }
    );
  }
}